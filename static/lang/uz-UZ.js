export default () => {
  return new Promise(function(resolve) {
    resolve({
      common: {
        title: 'A simple example',
        subtitle: "It's a blank-page. Start to build your'e page from here.",
        error_with_status: 'Serverda xatolik sodir bo\'ldi',
        '404': 'Bu sahifa topilmadi.',
        '404_subtitle':
          'Siz bu erda qolib, dam olishingiz yoki boshiga qaytishingiz mumkin.',
        back: 'bosh sahifaga qaytish',
        error_without_status: 'Serverda xatolik sodir bo\'ldi',
        contact_title: 'Reklama berish',
        outsourcing_title: 'Autsorsing',
        contact_subtitle:
          'Reklama berish uchun quyida keltirilgan ma\'lumotlarni ketma-ketlikda to\'ldiring va bizning mijozga aylaning!',
        outsourcing_subtitle: 'Quyidagi formani to\'ldirib bizning hamkorga aylaning.',
        outsourcing_material: 'Shablon faylni to\'ldirib bizga yuboring',
        outsourcing_template: 'Shablon',
        form_name: 'What is your name? *',
        form_email: 'What it your email? *',
        form_phone: 'What is your phone number?',
        form_company: 'What is your company?',
        form_message: 'Write your message here',
        form_terms: 'Shartnoma shartlari bilan tanishdim.',
        form_privacy: 'Service & Privacy Policy *',
        form_send: 'Send Message',
        accept: 'Accept',
        uz: 'O\'zbekcha',
        ru: 'Русский',
        notif_msg: 'This is an example of a message that is commonly used for important information to visitors or users. Please click the button beside to close this message.',
        where_place_add: "Qayerda reklama berasiz?",
        agreement: 'Shartnoma',
        payment: 'To\'lov',
        select_adv_type: 'Reklama turini tanlang:',
        select_contract: 'Reklama qiluvchu manbani tanlang:',
        select_adv_form: 'Reklama shaklini tanlang:',
        price: 'Narxi',
        soum: 'So\'m',
        continue: 'Davom etish',
        field_required: 'Majburiy maydon!',
        full_name: "To\'liq ism:",
        address: "Yashash manzili:",
        legal_entity: "Yuridik shaxs",
        individual: "Jismoniy shaxs",
        phone_number: "Telefon raqami:",
        passport_serial_number: "Pasport seriyasi va raqami:",
        passport_who_give: "Kim tomonidan berilgan:",
        go_back: "ortga",
        firm_name: "Tashkilot nomi:",
        director_fio: "Rahbarning ism sharifi:",
        firm_address: "Tashkilot manzili:",
        inn: "STIR:",
        bank_info: "Bank ma\'lumotlari:",
        mfo: "MFO:",
        must_agree: "Davom etish uchun rozilik bildirishingiz lozim!",
        order_id: "Buyurtma raqami",
        success_text: "Sizning buyurtmangiz muvafaqiyatli saqlandi. Endi to'lovni amalga oshirishingiz mumkin!",
        payment_text: "To'lovlar PayMe tizimi orqali amalga oshiriladi.",
        pay_btn_text: "To'lovni amalga oshirish",
        error_save: "Ma'lumotlarni saqlashda xatolik bo'ldi!",
        error_load: "Ma'lumot yuklashda xatolik bo'ldi!",
        add: "Qo'shish",
        day: "kun",
        days: "kun",
        remove: "O'chirish",
        company: "Tashklot:",
        send: "Yuborish",
        file: 'Fayl yuklang',
        successfully_saved: "Muvaffaqiyatli saqlandi!",
        hintForNote: "Eslatmada qo'shimcha talablaringizni kiriting!",
        note: "Eslatma",
        hintForAdditionalRequirement: "Qo'shimcha talablaringiz bo'lsa yozishingiz mumkin!",
        additionalRequirement:"Qo'shimcha talablar",
        advertisingDate: "Reklama joylashtirish sanasi"
      },
      agencyLanding: {
        header_about: 'Biz haqimizda',
        header_services: 'Xizmatlar',
        'header_our-expertise': 'Bizning tajriba',
        header_testimonials: 'guvohliklar',
        'header_case-studies': 'amaliy ishlar',
        header_our_office: 'bizni ofis',
        header_contact: 'Reklama berish',
        header_outsourcing: "Autsorsing",
        header_privacy: "Foydalanish qoidalari",
        header_language: 'til',
        header_theme: 'ko\'rinish',
        header_dark: 'qora',
        header_light: 'oq',
        banner_title: 'Reklama olamida uchamiz',
        banner_subtitle:
          'Barcha turdagi reklama xizmatlarini taklif qilamiz. Mijozlarimizga loyihalar kelajagini ta\'minlash va tovarlar sotuvida sezilarli natijalarga erishishida ko\'maklashamiz.',
        banner_button: 'reklama berish',
        about_title: 'Biz haqimizda',
        about_subtitle:
          'Katta imkoniyatlarimiz sabab sizning tanlovingizda ekanligimizni ayta olamiz!',
        about_employee: 'hamkorlar',
        about_projects: 'loyihalar',
        about_client: 'mijozlar',
        about_quote:
          "Reklama agentligi mijozning barcha reklama aspektlarini yaratadi, rejalashtiradi va boshqaradi.",
        services_title: 'Bizning xizmatlar',
        services_button: 'batafsil',
        expertise_title: 'Bizning tajriba',
        expertise_subtitle:
          'Muvaffaqiyatli loyiha - kuchli jamoa demakdir !',
        expertise_paragraph:
          'Biz reklama agentligi sifatida muayyan ixtisoslashgan sohalarni qamrab olamiz. Veb-saytlar, ijtimoiy tarmoqlar, reklama broshyuralari, kataloglar, pochta, radio va televideniye uchun maxsus reklama materiallarini yaratadigan to\'liq xizmat ko\'rsatuvchi agentlikmiz!',
        testimonial_title: 'Mijozlar fikri',
        case_title: 'Amaliy ishlar',
        cta_title: 'Boshlashga tayyormisiz?',
        cta_subtitle: 'Siz eng yaxshi agentlik xizmatidan foydalanmqodasiz...',
        cta_btn: 'Reklama berish',
        office_title: 'Bizning ofis',
        office_head: 'Bog\'lanish',
        office_branch: 'Branch Office',
        footer_paragraph:
          'Sizga yordam berish eng muhim maqsadimiz.'
      }
    })
  })
}
