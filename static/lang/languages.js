export default [
  {
    code: 'ru',
    iso: 'ru-RU',
    name: 'Русский',
    file: 'ru-RU.js',
    dir: 'ltr'
  },
  {
    code: 'uz',
    iso: 'uz-UZ',
    name: 'O\'bekcha',
    file: 'uz-UZ.js',
    dir: 'ltr'
  }
]
