const brand = {
  agency: {
    name: 'Uchamiz.uz',
    desc: 'uchamiz.uz - reklama olamida uchamiz!',
    prefix: 'uchamiz',
    footerText: 'uchamiz.uz - Barcha huquqlar homoyalangan '+new Date().getFullYear(),
    logoText: 'UCHAMIZ',
    projectName: 'uchamiz.uz',
    url: 'uchamiz.uz',
    img: '/static/images/adv-logo.png',
    notifMsg:
      'My notif msg'
  }
}

export default brand
