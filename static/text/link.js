const link = {
  agency: {
    home: '/',
    contact: '/contact',
    outsourcing: '/outsourcing',
    privacy: '/privacy'
  }
}

export default link
